/*
** keymap.h for rt1 in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/include
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Mon Feb 23 18:32:28 2015 Gabriel CADET
** Last update Mon Feb 23 18:35:28 2015 Gabriel CADET
*/

#ifndef KEYMAP_H_
# define KEYMAP_H_

/*
** First KB line (esc, F# and some function)
*/

# define KEY_F1	65470
# define KEY_F2	65471
# define KEY_F3	65472
# define KEY_F4	65473
# define KEY_F5	65474
# define KEY_F6	65475
# define KEY_F7	65476
# define KEY_F8	65477
# define KEY_F9	65478
# define KEY_F10	65479
# define KEY_F11	65480
# define KEY_F12	65481

/*
** Number's line
*/

# define KEY_EXP2	178
# define KEY_NB1	38
# define KEY_NB2	233
# define KEY_NB3	34
# define KEY_NB4	39
# define KEY_NB5	40
# define KEY_NB6	45
# define KEY_NB7	232
# define KEY_NB8	95
# define KEY_NB9	231
# define KEY_NB0	224
# define KEY_DEG	41
# define KEY_PLUS	61

/*
** Function key
*/

# define KEY_ARRDEF	65300
# define KEY_PAUSE	65299
# define KEY_INSER	66379
# define KEY_SUPPR	65535
# define KEY_DEL	65288
# define KEY_ESC	65307
# define KEY_RETURN	65293
# define KEY_SPACE	32
# define KEY_BEGIN	65360
# define KEY_PG_UP	65365
# define KEY_PG_DW	65566
# define KEY_END 	65366
# define KEY_TAB	65289
# define KEY_CAPS	65509
# define KEY_L_MAJ	65505
# define KEY_R_MAJ	65506
# define KEY_L_CTRL	65507
# define KEY_R_CTRL	65508
# define KEY_ALT	65513
# define KEY_ALT_GR	65027
# define KEY_MENU	65383
# define KEY_LARROW	65361
# define KEY_UARROW	65362
# define KEY_RARROW	65363
# define KEY_DARROW	65364

/*
** Letters
*/

# define KEY_A		97
# define KEY_B		98
# define KEY_C		99
# define KEY_D		100
# define KEY_E		101
# define KEY_F		102
# define KEY_G		103
# define KEY_H		104
# define KEY_I		105
# define KEY_J		106
# define KEY_K		107
# define KEY_L		108
# define KEY_M		109
# define KEY_N		110
# define KEY_O		111
# define KEY_P		112
# define KEY_Q		113
# define KEY_R		114
# define KEY_S		115
# define KEY_T		116
# define KEY_U		117
# define KEY_V		118
# define KEY_W		119
# define KEY_X		120
# define KEY_Y		121
# define KEY_Z		122

/*
** Others Char
*/

# define CIRCUM		65106
# define DOLLAR		36
# define MODULO		249
# define ASTERI		42
# define COMA		44
# define SEMICO		59
# define COLON		58
# define EXCLAM		35

typedef struct	s_key
{
  int		key;
  int		(*fct)();
}		t_key;

#endif /* !KEYMAP_H_ */
