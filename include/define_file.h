/*
** define_file.h for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs/parser
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Tue May 26 15:49:44 2015 Joffrey Mazier
** Last update Sun Jun  7 19:47:39 2015 Joffrey Mazier
*/

#ifndef DEFINE_FILE_H_
# define DEFINE_FILE_H_

# define NB_CARAC 17
# define M_PI 3.14159265359

# define OBJECT 1
# define CAMERA 2
# define LUX 3

# define SPHERE 1
# define CYLINDER 2
# define CONE 3
# define PLAN 4

# define UP 65362
# define DOWN 65364
# define LEFT 65361
# define RIGHT 65363
# define TABUL 65289
# define P_UP 65365
# define P_DOWN 65366
# define SQUARE_T 178
# define KEY_1 38
# define KEY_2 233
# define KEY_3 34
# define KEY_4 39
# define FASTER 61
# define NICER 41
# define EXPORT 120
# define SCREEN_S 65379

# define MIN_MODE 1
# define MAX_MODE 3

# define TRANSLATION 1
# define ROTATION 2
# define LIMITED 3
# define RESIZE 4

# define MODE(x) x == 1 ? "translation" : x == 2 ? "rotation" : "resize"
# define MODE_OBJ(x) x == 1 ? "object" : x == 2 ? "camera" : "spot"

# define AMOUNT "NB_OBJ"
# define NAME "NAME="

# define O_NAME 2
# define O_POS 4
# define O_VECTOR 8
# define O_COLOR 16
# define O_BRILLANCE 32
# define O_EXTRA 64
# define O_ANGLE 128
# define O_DIST 256
# define O_FOV 512
# define O_LIMIT 1024
# define O_LIMIT_MODE 2048
# define O_REFL 4096

#endif /* !DEFINE_FILE_H_ */
