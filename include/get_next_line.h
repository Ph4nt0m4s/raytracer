/*
** get_next_line.h for rtracer in /home/mazier_j/MUL_2014_rtracer
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Tue Jun  2 17:43:11 2015 Joffrey Mazier
** Last update Tue Jun  2 17:43:14 2015 Joffrey Mazier
*/

#ifndef GET_NEXT_LINE_H_
# define GET_NEXT_LINE_H_

# define SIZE_BUFF (10000)
# define SIZE (((SIZE_BUFF > 4294967293) || (SIZE_BUFF < 0)) ? 0 : SIZE_BUFF)

#endif /* !GET_NEXT_LINE_H_ */
