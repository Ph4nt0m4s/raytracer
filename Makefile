##
## Makefile for TP_mlx in /home/cadet_g/tp/TP_00
##
## Made by Gabriel CADET
## Login   <cadet_g@epitech.net>
##
## Started on  Mon Nov 17 11:00:41 2014 Gabriel CADET
## Last update Tue Jul 05 13:48:53 2016 Gabriel CADET
##

CC		= gcc

CFLAGS		+= -I include
CFLAGS		+= -W -Wall -Wextra 
CFLAGS		+= $(DEBUG)

LFLAGS		+= -L lib -lmlx -lXext -lX11 -lm -Ofas

RM		= rm -rf

NAME		= rt

LOGFOLD		= logs
ERRLOG		= 2> $(LOGFOLD)/$(shell basename $@).log
CLEANLOG	= if [ ! -s $(LOGFOLD)/$(shell basename $@).log ]; \
		  then rm $(LOGFOLD)/$(shell basename $@).log; fi
ECHO		= echo -e

DEFAULT	= "\033[00m"
GREEN	= "\033[0;32m"
TEAL	= "\033[1;36m"
RED	= "\033[0;31m"

SRCS	+= srcs/main.c
SRCS	+= srcs/math.c
SRCS	+= srcs/raytracing.c
SRCS	+= srcs/error.c
SRCS	+= srcs/init_coord.c
SRCS	+= srcs/endian.c
SRCS	+= srcs/init_event.c
SRCS	+= srcs/objects/make_objs.c
SRCS	+= srcs/eye/eye.c
SRCS	+= srcs/eye/eye2.c
SRCS	+= srcs/eye/rotate.c
SRCS	+= srcs/objects/sphere.c
SRCS	+= srcs/objects/rotate_obj.c
SRCS	+= srcs/objects/sphere2.c
SRCS	+= srcs/objects/plan.c
SRCS	+= srcs/objects/plan2.c
SRCS	+= srcs/objects/cone.c
SRCS	+= srcs/objects/cone2.c
SRCS	+= srcs/objects/cylinder.c
SRCS	+= srcs/objects/cylinder2.c
SRCS	+= srcs/objects/inter_sphere.c
SRCS	+= srcs/objects/inter_plan.c
SRCS	+= srcs/objects/inter_cylinder.c
SRCS	+= srcs/objects/inter_cone.c
SRCS	+= srcs/objects/destroy_objs.c
SRCS	+= srcs/lights/get_light.c
SRCS	+= srcs/lights/get_normal.c
SRCS	+= srcs/lights/init_spot.c
SRCS	+= srcs/lights/manage_color.c
SRCS	+= srcs/mlx_utils/gere_hook.c
SRCS	+= srcs/mlx_utils/gere_resize.c
SRCS	+= srcs/mlx_utils/put_pxl_to_img.c
SRCS	+= srcs/mlx_utils/export_conf.c
SRCS	+= srcs/mlx_utils/gere_obj.c
SRCS	+= srcs/mlx_utils/gere_cam.c
SRCS	+= srcs/mlx_utils/gere_lux.c
SRCS	+= srcs/mlx_utils/screenshot_module.c
SRCS	+= srcs/mlx_utils/create_export_file.c
SRCS	+= srcs/mlx_utils/fill_export_file.c
SRCS	+= srcs/utils/display.c
SRCS	+= srcs/xfiles/xmalloc.c
SRCS	+= srcs/xfiles/xwrite.c
SRCS	+= srcs/parser/get_next_line.c
SRCS	+= srcs/parser/parce_partoo.c
SRCS	+= srcs/parser/gt_obj_carac.c
SRCS	+= srcs/parser/gt_obj_carac2.c
SRCS	+= srcs/parser/gt_end.c
SRCS	+= srcs/parser/gt_obj.c
SRCS	+= srcs/parser/gt_pos.c
SRCS	+= srcs/parser/set_default.c
SRCS	+= srcs/parser/gt_cam.c
SRCS	+= srcs/parser/gt_name.c
SRCS	+= srcs/parser/gt_limit.c
SRCS	+= srcs/parser/init_data.c
SRCS	+= srcs/parser/init_tab.c
SRCS	+= srcs/parser/utils_pars.c
SRCS	+= srcs/parser/utils_pars2.c

OBJS	= $(SRCS:.c=.o)

MAKELIB	= make -C

all: mlx $(NAME)

%.o: %.c
	@mkdir -p $(LOGFOLD)
	@-$(CC) -c $< -o $@ $(DEBUG) $(CFLAGS) \
	 $(ERRLOG) && \
	 $(ECHO) $(GREEN) "[OK]" $(TEAL) $< $(DEFAULT) || \
	 $(ECHO) $(RED) "[XX]" $(TEAL) $< $(DEFAULT)
	@$(CLEANLOG)

$(NAME): $(OBJS)
	@-$(CC) $(OBJS) -o $(NAME) $(LFLAGS) \
	 $(ERRLOG) && \
	 $(ECHO) $(GREEN) "[OK]" $(TEAL) $@ $(DEFAULT) || \
	 $(ECHO) $(RED) "[XX]" $(TEAL) $@ $(DEFAULT)
	@$(CLEANLOG)

mlx:
	$(MAKELIB) lib/minilibx
	cp lib/minilibx/mlx.h include
	cp lib/minilibx/libmlx.a lib

clean:
	@-$(RM) $(OBJS)
	@-$(ECHO) $(TEAL) "cleaning object files" $(DEFAULT)

clean_log:
	@-$(RM) $(LOGFOLD)
	@-$(ECHO) $(TEAL) "cleaning logfiles" $(DEFAULT)

fclean: clean clean_log
	@-$(RM) $(NAME)
	@-$(ECHO) $(TEAL) "Removing binary" $(DEFAULT)
	$(MAKELIB) lib/minilibx/ fclean
	$(RM) lib/libmlx.a
	$(RM) include/mlx.h

re: fclean all

.PHONY: all clean clean_log fclean re
