/*
** endian.c for endian in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Mon Mar  9 23:31:33 2015 Gabriel CADET
** Last update Mon Mar  9 23:34:39 2015 Gabriel CADET
*/

int	is_little_endian(void)
{
  short	nbr;
  char	*know;

  nbr = 1;
  know = (char *) &nbr;
  return (*know);
}
