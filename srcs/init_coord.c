/*
** init_coord.c for rtv1 in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/srcs
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Wed Mar 11 18:51:06 2015 Gabriel CADET
** Last update Thu Mar 12 00:48:47 2015 Gabriel CADET
*/

#include "rt1.h"

void	init_color(int *color)
{
  color[0] = 0;
  color[1] = 0;
  color[2] = 0;
}

void	init_lumin(double *color)
{
  color[0] = 0;
  color[1] = 0;
  color[2] = 0;
}
