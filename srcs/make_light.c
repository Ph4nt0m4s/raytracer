/*
** make_light.c for rt1 in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Mon Mar  9 23:43:15 2015 Gabriel CADET
** Last update Thu Mar 12 17:55:56 2015 Gabriel CADET
*/

#include <stdlib.h>
#include "xfiles.h"
#include "rt1.h"

t_spot	*spot1(int color)
{
  double	coord[3];

  coord[0] = 50;
  coord[1] = 0;
  coord[2] = 75;
  return (init_spot(coord, color));
}

t_spot	*spot2(int color)
{
  double	coord[3];

  coord[0] = -50;
  coord[1] = 50;
  coord[2] = 75;
  return (init_spot(coord, color));
}

t_spot	*spot3(int color)
{
  double	coord[3];

  coord[0] = -50;
  coord[1] = -50;
  coord[2] = 75;
  return (init_spot(coord, color));
}

t_spot	*spot4(int color)
{
  double	coord[3];

  coord[0] = -200;
  coord[1] = 0;
  coord[2] = 100;
  return (init_spot(coord, color));
}

t_spot		**make_light()
{
  t_spot	**spots;

  if (!(spots = xmalloc(sizeof(t_spot *) * 9)))
    return (NULL);
  if (!(spots[0] = spot1(0xFF0000)))
    return (NULL);
  if (!(spots[1] = spot2(0x00FF00)))
    return (NULL);
  if (!(spots[2] = spot3(0x0000FF)))
    return (NULL);
  if (!(spots[3] = spot4(0xFFFFFF)))
    return (NULL);
  if (!(spots[4] = spot1(0xFFFFFF)))
    return (NULL);
  if (!(spots[5] = spot2(0xFFFFFF)))
    return (NULL);
  if (!(spots[6] = spot3(0xFFFFFF)))
    return (NULL);
  if (!(spots[7] = spot4(0xFFFFFF)))
    return (NULL);
  spots[8] = NULL;
  return (spots);
}
