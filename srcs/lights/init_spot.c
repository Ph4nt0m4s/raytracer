/*
** init_spot.c for rtv1 in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs/lights
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Mon Mar  9 20:44:02 2015 Gabriel CADET
** Last update Mon Jun  1 17:58:15 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include "rt1.h"
#include "xfiles.h"

t_spot		*init_spot(double *coord, int color)
{
  t_spot	*spot;

 if (!(spot = xmalloc(sizeof(t_spot))))
   return (NULL);
 spot->x = coord[0];
 spot->y = coord[1];
 spot->z = coord[2];
 spot->color = color;
 return (spot);
}
