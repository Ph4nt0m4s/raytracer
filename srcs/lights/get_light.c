/*
** get_light.c for rt1 in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs/lights
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Mon Mar  9 20:56:27 2015 Gabriel CADET
** Last update Sun Jun  7 18:58:15 2015 Joffrey Mazier
*/

#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <math.h>
#include "rt1.h"

double		is_inter(double *coord, t_obj **objs, t_spot *spot)
{
  t_eye		emul_eye;
  int		i;
  double	k;
  double	tmp;

  i = -1;
  k = 1;
  emul_eye.x = coord[0];
  emul_eye.y = coord[1];
  emul_eye.z = coord[2];
  emul_eye.vx = spot->x - coord[0];
  emul_eye.vy = spot->y - coord[1];
  emul_eye.vz = spot->z - coord[2];
  while (objs[++i])
    if ((tmp = objs[i]->inter(&emul_eye, objs[i])) > 0.00000000001)
      k = MIN(k, tmp);
  return (k);
}

double		get_angle(double *coord, t_obj *obj, t_spot *spot)
{
  double	normal[3];
  double	norm_obj;
  double	vlight[3];
  double	norm_light;
  double	scal;
  double	cos_a;

  obj->get_normal(obj, normal, coord);
  vlight[0] = spot->x - coord[0];
  vlight[1] = spot->y - coord[1];
  vlight[2] = spot->z - coord[2];
  norm_obj = calc_norm(normal);
  norm_light = calc_norm(vlight);
  scal = calc_scalaire(normal, vlight);
  cos_a = scal / (norm_obj * norm_light);
  if (cos_a < 0)
    return (0.0000000000);
  return (cos_a);
}

int		calc_lumin(double *coef, double angle, int *light)
{
  int		i;

  i = -1;
  while (++i < 3)
    coef[i] += angle * (double) (light[i] / 255);
  return (0);
}

int		calc_brillance(int *col, double coef, double angle, int *light)
{
  int		i;

  i = -1;
  while (++i < 3)
    col[i] += (angle * coef * (double) light[i]);
  return (0);
}

int		get_lumin(double *pts, t_obj **objs, t_obj *obj, t_spot **spot)
{
  int		i;
  double	angle;
  double	inter;
  int		color[3];
  int		light_color[3];
  double	lumin[3];
  int		nspot;

  i = -1;
  nspot = 0;
  init_color(color);
  init_lumin(lumin);
  if (!spot)
    return (0);
  while (spot[++i] && ++nspot)
    if (((angle = get_angle(pts, obj, spot[i])) > 0. && angle <= 1.) &&
	((inter = is_inter(pts, objs, spot[i])) < 0 || inter >= 1))
      {
	light_to_int(spot[i], light_color);
	calc_brillance(color, obj->brillance, angle, light_color);
	calc_lumin(lumin, angle, light_color);
      }
  apply_color(obj, color, lumin, nspot);
  return (make_color(color));
}
