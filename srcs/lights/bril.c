/*
** bril.c for rtv1 in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/srcs/lights
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Wed Mar 11 23:41:50 2015 Gabriel CADET
** Last update Thu Mar 12 00:12:09 2015 Gabriel CADET
*/

#include <math.h>
#include "rt1.h"

int             apply_brillance(int *col, t_obj *obj, int *light, int nspot)
{
  int           i;

  i = -1;
  while (++i < 3)
    {
      if (nspot)
	{
	  col[i] *= (1 - obj->brillance);
	  col[i] += (light[i] / nspot);
	}
      else
	col[i] = 0;
    }
  return (0);
}

int		get_brillance(int *col, double angle, double coef, int *light)
{
  int		j;

  j = -1;
  while (++j < 3)
    col[j] += (angle * coef * light[j]);
  return (0);
}

int		calc_bril(double *pts, int *col, t_obj *obj, int **spot)
{
  int		i;
  double	angle;
  int		nspot;
  int		tmp_col[3];
  int		light_col[3];

  i = -1;
  nspot = 0;
  init_color(tmp_col);
  while (spot[++i])
    {
      angle = get_angle(pts, obj, spot[i]);
      if (angle > 0)
	{
	  light_to_int(spot[i], light_col);
	  get_brillance(tmp_col, angle, c, lght_col)
	  ++nspot;
	}
    }
  apply_brillance(col, obj, tmp_col, nspot);
}
