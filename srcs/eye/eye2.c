/*
** eye.c for raytracer in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs/objects
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Tue Feb 24 00:21:13 2015 Gabriel CADET
** Last update Sun Mar 22 20:58:38 2015 Gabriel CADET
*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "rt1.h"

int		eye_vector_y(t_eye *eye, int x, int y)
{
  double	angle;

  (void) y;
  angle = (eye->fov / XWIN) * ((XWIN / 2) - (double) x);
  eye->vy = eye->dist * tan(angle);
  return (0);
}

int		eye_vector_z(t_eye *eye, int x, int y)
{
  double	angle;

  (void) x;
  angle = (eye->fov / YWIN) *((YWIN / 2) - (double) y);
  eye->vz = eye-> dist * tan(angle);
  return (0);
}
