/*
** rotate.c for rtv1 in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs/objects
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Fri Feb 27 08:42:39 2015 Gabriel CADET
** Last update Fri Jun  5 22:50:03 2015 Joffrey Mazier
*/

#include <math.h>
#include "rt1.h"

double		rota_x(t_eye *eye, double angle)
{
  double	y_tmp;
  double	z_tmp;

  y_tmp = eye->vy;
  z_tmp = eye->vz;
  eye->vy = y_tmp * cos(angle) - z_tmp * sin(angle);
  eye->vz = y_tmp * sin(angle) + z_tmp * cos(angle);
  return (0);
}

double		rota_y(t_eye *eye, double angle)
{
  double	x_tmp;
  double	z_tmp;

  x_tmp = eye->vx;
  z_tmp = eye->vz;
  eye->vz = z_tmp * cos(angle) - x_tmp * sin(angle);
  eye->vx = z_tmp * sin(angle) + x_tmp * cos(angle);
  return (0);
}

double		rota_z(t_eye *eye, double angle)
{
  double	x_tmp;
  double	y_tmp;

  x_tmp = eye->vx;
  y_tmp = eye->vy;
  eye->vx = x_tmp * cos(angle) - y_tmp * sin(angle);
  eye->vy = x_tmp * sin(angle) + y_tmp * cos(angle);
  return (0);
}
