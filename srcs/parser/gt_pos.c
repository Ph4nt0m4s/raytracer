/*
** gt_pos.c for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs/parser
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Fri Jun  5 21:51:17 2015 Joffrey Mazier
** Last update Fri Jun  5 22:05:28 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include "define_file.h"
#include "rt1.h"

int		check_value_pos(char *str)
{
  int		i;

  i = 0;
  while (str[i] != '\0')
    {
      if ((str[i] < '0' || str[i] > '9') && str[i] != '-' && str[i] != '.')
        return (-1);
      i++;
    }
  return (0);
}

void		gt_pos2(t_pars *pars, int i, char *str)
{
  if (pars->ext == OBJECT && i == 0)
    pars->obj[pars->obj_n[pars->ext]]->x = atof(str);
  else if (pars->ext == OBJECT && i == 1)
    pars->obj[pars->obj_n[pars->ext]]->y = atof(str);
  else if (pars->ext == OBJECT && i == 2)
    pars->obj[pars->obj_n[pars->ext]]->z = atof(str);
  if (pars->ext == LUX && i == 0)
    pars->spot[pars->obj_n[pars->ext]]->x = atof(str);
  else if (pars->ext == LUX && i == 1)
    pars->spot[pars->obj_n[pars->ext]]->y = atof(str);
  else if (pars->ext == LUX && i == 2)
    pars->spot[pars->obj_n[pars->ext]]->z = atof(str);
  if (pars->ext == CAMERA && i == 0)
    pars->eye[pars->obj_n[pars->ext]]->x = atof(str);
  else if (pars->ext == CAMERA && i == 1)
    pars->eye[pars->obj_n[pars->ext]]->y = atof(str);
  else if (pars->ext == CAMERA && i == 2)
    pars->eye[pars->obj_n[pars->ext]]->z = atof(str);
}

int		gt_pos(t_pars *pars)
{
  int		i;
  char		*str;

  i = 0;
  if (pars->obj_n[pars->ext] < 0)
    return (0);
  while (i < pars->get_carac[pars->tag_type])
    {
      if ((str = get_next_line(pars->fd, 0)) == NULL)
        return (0);
      if ((check_value_pos(str)) == -1)
        return (0);
      gt_pos2(pars, i, str);
      i++;
      free(str);
    }
  pars->mask ^= O_POS;
  return (0);
}
