/*
** parce_partoo.c for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs/parser
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Tue May 26 15:14:08 2015 Joffrey Mazier
** Last update Sat Jun  6 17:23:20 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "rt1.h"
#include "define_file.h"

int		(*tab_tag[NB_CARAC])(t_pars *pars) =
{
  gt_sphere,
  gt_cylindre,
  gt_cone,
  gt_plan,
  gt_pos,
  gt_color,
  gt_brillance,
  gt_fov,
  gt_dist,
  gt_angle,
  gt_name,
  gt_vector,
  gt_end,
  gt_extra,
  gt_limit,
  gt_limit_mode,
  gt_refl
};

int		full_fill_struct2(t_pars *pars, int times, char *str, int i)
{
  if (times == 2)
    {
      if ((init_data_struct(pars)) == -1)
        return (-1);
    }
  else if (times == 0)
    {
      if ((check_file_type(str, pars)) == -1)
        return (-1);
    }
  else if (times == 1)
    {
      if ((pars->nb_obj = check_nb_objects(str)) == -1)
        return (-1);
    }
  else
    while (i < NB_CARAC)
      if ((my_str_compare(str, pars->carac[i++])) == 1)
        {
          pars->tag_type = i - 1;
          if ((tab_tag[pars->tag_type](pars)) == -1)
            return (-1);
          break ;
        }
  return (0);
}

int		full_fill_struct(t_pars *pars)
{
  int		fd;
  char		*str;
  int		times;
  int		i;

  times = 0;
  pars->obj_n[0] = -1;
  pars->obj_n[OBJECT] = -1;
  pars->obj_n[CAMERA] = -1;
  pars->obj_n[LUX] = -1;
  if ((fd = open(pars->path, O_RDONLY)) == -1)
    return (my_puterror("Error with open func()\n"));
  pars->fd = fd;
  while ((str = get_next_line(pars->fd, 0)))
    {
      i = 0;
      if ((full_fill_struct2(pars, times, str, i)) == -1)
        return (-1);
      times++;
      free(str);
    }
  close(fd);
  return (0);
}

t_pars		*get_next_file(t_pars *pars, char *path_dir)
{
  struct dirent	*info;
  DIR		*dir;

  if ((init_carac_tab(pars)) == -1)
    return (NULL);
  if ((dir = opendir(path_dir)) == NULL)
    {
      my_puterror("Error with opendir, check your directory's path !\n");
      return (NULL);
    }
  while ((info = readdir(dir)) != 0)
    {
      if ((pars->ext = get_file_type(info->d_name)) != -1)
	if ((pars->path = make_the_path(path_dir, info->d_name)) != NULL)
	  if ((full_fill_struct(pars)) == -1)
	    return (NULL);
    }
  if ((closedir(dir)) == -1)
    return (NULL);
  if (pars->nb_object > 0 && pars->nb_cam > 0 && pars->nb_lux > 0)
    return (pars);
  my_puterror("There is not enought objects or/and spots or/and camera !\n");
  return (NULL);
}

t_pars		*init_parsing(char *path_dir, t_obj **obj,
			     t_spot **spot, t_eye **eye)
{
  t_pars	*pars;

  if ((pars = malloc((sizeof(t_pars)) * 1)) == NULL)
    {
      my_puterror("Error with malloc(), pars\n");
      return (NULL);
    }
  pars->end = 1;
  pars->mask = 0;
  pars->type = -1;
  pars->obj = obj;
  pars->spot = spot;
  pars->eye = eye;
  pars->nb_object = 0;
  pars->nb_cam = 0;
  pars->nb_lux = 0;
  if ((pars = get_next_file(pars, path_dir)) != NULL)
    return (pars);
  return (NULL);
}
