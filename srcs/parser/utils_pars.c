/*
** utils_pars.c for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs/parser
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Fri Jun  5 22:13:51 2015 Joffrey Mazier
** Last update Fri Jun  5 22:30:53 2015 Joffrey Mazier
*/

#include <unistd.h>
#include <stdlib.h>
#include "rt1.h"
#include "./define_file.h"

int		my_puterror(char *str)
{
  int		i;

  i = 0;
  while (str[i] != '\0')
    write(2, &str[i++], 1);
  return (-1);
}

int		my_nstrlen(unsigned char *str, unsigned char c)
{
  unsigned long i;

  i = 0;
  if (str != NULL)
    while (str[i] != '\0' && str[i] != c)
      ++i;
  return (i);
}

int		my_str_compare(char *str, char *comp)
{
  int		i;

  i = 0;
  if (my_strlen(str) != my_strlen(comp))
    return (-1);
  while (str[i] != '\0')
    {
      if (str[i] != comp[i])
        return (-1);
      i++;
    }
  return (1);
}

int		get_file_type2(char *ext)
{
  if ((my_str_compare(ext, "obj")) == 1)
    {
      free(ext);
      return (OBJECT);
    }
  else if ((my_str_compare(ext, "cam")) == 1)
    {
      free(ext);
      return (CAMERA);
    }
  else if ((my_str_compare(ext, "lux")) == 1)
    {
      free(ext);
      return (LUX);
    }
  free(ext);
  return (-1);
}

int		get_file_type(char *file)
{
  int		i;
  int		j;
  char		*ext;

  i = 0;
  j = 0;
  while (file[i] != '\0')
    i++;
  if ((ext = malloc((sizeof(char)) * (i + 2))) == NULL)
    return (my_puterror("Error with malloc(), ext\n"));
  while (i >= 0 && file[i] != '.')
    i--;
  if (file[i] == '.' && file[i + 1] != '\0')
    i++;
  while (file[i] != '\0')
    ext[j++] = file[i++];
  ext[j] = '\0';
  return (get_file_type2(ext));
}
