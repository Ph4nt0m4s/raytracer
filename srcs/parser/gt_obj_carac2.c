/*
** gt_obj_carac2.c for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs/parser
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Sat Jun  6 17:10:59 2015 Joffrey Mazier
** Last update Sat Jun  6 17:12:18 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include "rt1.h"
#include "define_file.h"

int		gt_refl(t_pars *pars)
{
  char		*str;

  if (pars->obj_n[pars->ext] < 0)
    return (0);
  if ((str = get_next_line(pars->fd, 0)) == NULL)
    return (0);
  if (pars->ext == OBJECT)
    pars->obj[pars->obj_n[pars->ext]]->refl = atof(str);
  else
    return (my_puterror("Error. Value REFL is not needed\
 for this object..\n"));
  free(str);
  pars->mask ^= O_REFL;
  return (0);
}
