/*
** set_default.c for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs/parser
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Fri Jun  5 21:52:42 2015 Joffrey Mazier
** Last update Sat Jun  6 17:12:54 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include "define_file.h"
#include "rt1.h"

void		set_cam_default2(t_pars *pars)
{
  if (!(pars->mask & O_FOV))
    {
      my_puterror("Warning, FOV is lacking and was set by default !\n");
      pars->eye[pars->obj_n[pars->ext]]->fov = 100;
    }
  if (!(pars->mask & O_DIST))
    {
      my_puterror("Warning, DIST is lacking and was set by default !\n");
      pars->eye[pars->obj_n[pars->ext]]->dist = 100;
    }
  if (!(pars->mask & O_ANGLE))
    {
      my_puterror("Warning, ANGLE is lacking and was set by default !\n");
      pars->eye[pars->obj_n[pars->ext]]->anglex = 1;
      pars->eye[pars->obj_n[pars->ext]]->angley = 1;
      pars->eye[pars->obj_n[pars->ext]]->anglez = 1;
    }
}

void		set_cam_default(t_pars *pars)
{
  if (!(pars->mask & O_NAME))
    {
      my_puterror("Warning, NAME is lacking and was set by default !\n");
      pars->eye[pars->obj_n[pars->ext]]->name = "default";
    }
  if (!(pars->mask & O_POS))
    {
      my_puterror("Warning, POS is lacking and was set by default !\n");
      pars->eye[pars->obj_n[pars->ext]]->x = -200;
      pars->eye[pars->obj_n[pars->ext]]->y = 0;
      pars->eye[pars->obj_n[pars->ext]]->z = 40;
    }
  set_cam_default2(pars);
}

void		set_lux_default(t_pars *pars)
{
  if (!(pars->mask & O_NAME))
    {
      my_puterror("Warning, NAME is lacking and was set by default !\n");
      pars->spot[pars->obj_n[pars->ext]]->name = "default";
    }
  if (!(pars->mask & O_POS))
    {
      my_puterror("Warning, POS is lacking and was set by default !\n");
      pars->spot[pars->obj_n[pars->ext]]->x = 0;
      pars->spot[pars->obj_n[pars->ext]]->y = 0;
      pars->spot[pars->obj_n[pars->ext]]->z = 0;
    }
  if (!(pars->mask & O_COLOR))
    {
      my_puterror("Warning, COLOR is lacking and was set by default !\n");
      pars->spot[pars->obj_n[pars->ext]]->color = strtol("0x0A0A0A", NULL, 16);
    }
}

void		set_obj_default2(t_pars *pars)
{
  if (!(pars->mask & O_COLOR))
    {
      my_puterror("Warning, COLOR is lacking and was set by default !\n");
      pars->obj[pars->obj_n[pars->ext]]->color = strtol("0x0A0A0A", NULL, 16);
    }
  if (!(pars->mask & O_BRILLANCE))
    {
      my_puterror("Warning, BRILLANCE is lacking and was set by default !\n");
      pars->obj[pars->obj_n[pars->ext]]->brillance = 0.5;
    }
  if (!(pars->mask & O_EXTRA))
    {
      my_puterror("Warning, EXTRA is lacking and was set by default !\n");
      pars->obj[pars->obj_n[pars->ext]]->extra = 20;
    }
  if (!(pars->mask & O_VECTOR))
    {
      my_puterror("Warning, VECTOR is lacking and was set by default !\n");
      pars->obj[pars->obj_n[pars->ext]]->vx = 0;
      pars->obj[pars->obj_n[pars->ext]]->vy = 0;
      pars->obj[pars->obj_n[pars->ext]]->vz = 0;
    }
}

int		set_obj_default(t_pars *pars)
{
  if (pars->type == -1)
    return (my_puterror("Invalid object !\n"));
  if (!(pars->mask & O_NAME))
    {
      my_puterror("Warning, NAME is lacking and was set by default !\n");
      pars->obj[pars->obj_n[pars->ext]]->name = "default";
    }
  if (!(pars->mask & O_POS))
    {
      my_puterror("Warning, POS is lacking and was set by default !\n");
      pars->obj[pars->obj_n[pars->ext]]->x = 0;
      pars->obj[pars->obj_n[pars->ext]]->y = 0;
      pars->obj[pars->obj_n[pars->ext]]->z = 0;
    }
  if (!(pars->mask & O_LIMIT))
    pars->obj[pars->obj_n[pars->ext]]->limit[0] = 0;
  if (!(pars->mask & O_LIMIT_MODE))
    pars->obj[pars->obj_n[pars->ext]]->limit[1] = 0;
  if (!(pars->mask & O_REFL))
    pars->obj[pars->obj_n[pars->ext]]->refl = 0;
  set_obj_default2(pars);
  return (0);
}
