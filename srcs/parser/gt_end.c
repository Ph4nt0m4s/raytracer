/*
** gt_end.c for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs/parser
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Fri Jun  5 21:47:31 2015 Joffrey Mazier
** Last update Sat Jun  6 18:50:36 2015 Joffrey Mazier
*/

#include "define_file.h"
#include "rt1.h"

int		gt_end_cam(t_pars *pars)
{
  double	coord[3];
  double	angle[3];
  t_eye		*tmp;

  pars->nb_cam++;
  coord[0] = pars->eye[pars->obj_n[pars->ext]]->x;
  coord[1] = pars->eye[pars->obj_n[pars->ext]]->y;
  coord[2] = pars->eye[pars->obj_n[pars->ext]]->z;
  angle[0] = pars->eye[pars->obj_n[pars->ext]]->anglex;
  angle[1] = pars->eye[pars->obj_n[pars->ext]]->angley;
  angle[2] = pars->eye[pars->obj_n[pars->ext]]->anglez;
  if ((tmp = make_eye(coord,
                      pars->eye[pars->obj_n[pars->ext]]->dist,
                      pars->eye[pars->obj_n[pars->ext]]->fov,
                      angle)) == NULL)
    return (my_puterror("Error with make_eye() func !\n"));
  tmp->name = pars->eye[pars->obj_n[pars->ext]]->name;
  pars->eye[pars->obj_n[pars->ext]] = tmp;
  return (0);
}

int		gt_end_lux(t_pars *pars)
{
  int		color;
  double	coord[3];
  t_spot	*tmp;

  pars->nb_lux++;
  color = pars->spot[pars->obj_n[pars->ext]]->color;
  coord[0] = pars->spot[pars->obj_n[pars->ext]]->x;
  coord[1] = pars->spot[pars->obj_n[pars->ext]]->y;
  coord[2] = pars->spot[pars->obj_n[pars->ext]]->z;
  tmp = init_spot(coord, color);
  tmp->name = pars->spot[pars->obj_n[pars->ext]]->name;
  pars->spot[pars->obj_n[pars->ext]] = tmp;
  return (0);
}

void		init_end_obj(t_pars *pars, double *coord,
			     double *extra, double *vector)
{
  coord[0] = pars->obj[pars->obj_n[pars->ext]]->x;
  coord[1] = pars->obj[pars->obj_n[pars->ext]]->y;
  coord[2] = pars->obj[pars->obj_n[pars->ext]]->z;
  extra[0] = pars->obj[pars->obj_n[pars->ext]]->brillance;
  vector[0] = pars->obj[pars->obj_n[pars->ext]]->vx;
  vector[1] = pars->obj[pars->obj_n[pars->ext]]->vy;
  vector[2] = pars->obj[pars->obj_n[pars->ext]]->vz;
}

void		gt_end_obj(t_pars *pars)
{
  double	coord[3];
  double	extra[2];
  double	vector[3];
  t_obj		*tmp;
  double	color;

  pars->nb_object++;
  color = pars->obj[pars->obj_n[pars->ext]]->color;
  init_end_obj(pars, coord, extra, vector);
  if (pars->type == CYLINDER || pars->type == CONE || pars->type == SPHERE)
    extra[1] =  pars->obj[pars->obj_n[pars->ext]]->extra;
  if (pars->type == CYLINDER)
    tmp = make_cylinder(coord, vector, color, extra);
  else if (pars->type == CONE)
    tmp = make_cone(coord, vector, color, extra);
  else if (pars->type == PLAN)
    tmp = make_plan(coord, vector, color, extra);
  else if (pars->type == SPHERE)
    tmp = make_sphere(coord, vector, color, extra);
  tmp->name = pars->obj[pars->obj_n[pars->ext]]->name;
  tmp->limit[0] = pars->obj[pars->obj_n[pars->ext]]->limit[0];
  tmp->limit[1] = pars->obj[pars->obj_n[pars->ext]]->limit[1];
  tmp->refl = pars->obj[pars->obj_n[pars->ext]]->refl;
  pars->obj[pars->obj_n[pars->ext]] = tmp;
  pars->obj[pars->obj_n[pars->ext] + 1] = NULL;
}

int		gt_end(t_pars *pars)
{
  pars->end = 1;
  if (pars->obj_n[pars->ext] < 0)
    pars->obj_n[pars->ext] = 0;
  if (pars->ext == LUX)
    {
      set_lux_default(pars);
      gt_end_lux(pars);
    }
  else if (pars->ext == CAMERA)
    {
      set_cam_default(pars);
      gt_end_cam(pars);
    }
  else if (pars->ext == OBJECT)
    {
      if ((set_obj_default(pars)) == -1)
        return (-1);
      gt_end_obj(pars);
    }
  pars->mask = 0;
  pars->type = -1;
  return (0);
}
