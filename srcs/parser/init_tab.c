/*
** init_tab.c for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs/parser
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Fri Jun  5 22:13:33 2015 Joffrey Mazier
** Last update Sat Jun  6 17:09:55 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include "rt1.h"
#include "./define_file.h"

int		init_gtcarac_tab(t_pars *pars)
{
  if ((pars->get_carac = malloc((sizeof(int)) * (NB_CARAC + 2))) == NULL)
    return (my_puterror("Error with malloc(), get_carac\n"));
  pars->get_carac[0] = -1;
  pars->get_carac[1] = -2;
  pars->get_carac[2] = -3;
  pars->get_carac[3] = -4;
  pars->get_carac[4] = 3;
  pars->get_carac[5] = 3;
  pars->get_carac[6] = 1;
  pars->get_carac[7] = 1;
  pars->get_carac[8] = 1;
  pars->get_carac[9] = 3;
  pars->get_carac[10] = -50;
  pars->get_carac[11] = 3;
  pars->get_carac[12] = 0;
  pars->get_carac[13] = 1;
  pars->get_carac[14] = 1;
  pars->get_carac[15] = 1;
  pars->get_carac[16] = 1;
  return (0);
}

int		init_carac_tab(t_pars *pars)
{
  if ((pars->carac = malloc((sizeof(char*)) * (NB_CARAC + 2))) == NULL)
    return (my_puterror("Error with malloc(), pars->carac\n"));
  pars->carac[0] = "## SPHERE";
  pars->carac[1] = "## CYLINDRE";
  pars->carac[2] = "## CONE";
  pars->carac[3] = "## PLAN";
  pars->carac[4] = "## POS";
  pars->carac[5] = "## COLOR";
  pars->carac[6] = "## BRILLANCE";
  pars->carac[7] = "## FOV";
  pars->carac[8] = "## DIST";
  pars->carac[9] = "## ANGLE";
  pars->carac[10] = "## NAME";
  pars->carac[11] = "## VECTOR";
  pars->carac[12] = "## END";
  pars->carac[13] = "## EXTRA";
  pars->carac[14] = "## LIMIT";
  pars->carac[15] = "## LIMIT_MODE";
  pars->carac[16] = "## REFL";
  pars->carac[17] = "NULL";
  if ((init_gtcarac_tab(pars)) == -1)
    return (-1);
  return (0);
}
