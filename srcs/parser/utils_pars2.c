/*
** utils_pars2.c for raytracing in /home/mazier_j/MUL_2014_rtracer/srcs/parser
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Fri Jun  5 22:28:44 2015 Joffrey Mazier
** Last update Fri Jun  5 22:34:09 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include "rt1.h"
#include "define_file.h"

int		check_file_type(char *str, t_pars *pars)
{
  (void)str;
  (void)pars;
  return (0);
}

char		*make_the_path(char *path_dir, char *file)
{
  char		*file_path;
  int		i;
  int		j;

  i = 0;
  j = 0;
  if ((file_path = malloc((sizeof(char))
                          + (my_strlen(path_dir)
                             + my_strlen(file) + 2))) == NULL)
    return (NULL);
  while (path_dir[i] != '\0')
    file_path[j++] = path_dir[i++];
  if (file_path[j - 1] != '/')
    file_path[j++] = '/';
  i = 0;
  while (file[i] != '\0')
    file_path[j++] = file[i++];
  file_path[j] = '\0';
  return (file_path);
}

int		check_nb_objects(char *str)
{
  int		i;
  int		j;
  char		*nb;
  int		num;

  i = 0;
  j = 0;
  if ((nb = malloc((sizeof(char)) * my_strlen(str))) == NULL)
    return (my_puterror("Error with malloc(), nb\n"));
  while (str[i] != '\0' && str[i] != '=')
    i++;
  if (str[i] == '=')
    {
      while (str[i] != '\0' && (str[i] < 49 || str[i] > 57))
        i++;
      while (str[i] != '\0' && (str[i] >= 49 && str[i] <= 57))
        nb[j++] = str[i++];
      nb[j] = '\0';
      num = atoi(nb);
      free(nb);
      return (num);
    }
  return (-1);
}
