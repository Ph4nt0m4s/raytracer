/*
** gt_obj.c for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs/parser
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Fri Jun  5 21:50:12 2015 Joffrey Mazier
** Last update Sun Jun  7 18:57:52 2015 Joffrey Mazier
*/

#include "define_file.h"
#include "rt1.h"

int		gt_sphere(t_pars *pars)
{
  if (pars->end == 1)
    if (++pars->obj_n[pars->ext] > pars->nb_obj)
      return (my_puterror("Error. There is more object\
 in your file than you told us. Liar.\n"));
  if (pars->ext != OBJECT)
    return (my_puterror("Error, it's not an object' file !\n"));
  pars->type = SPHERE;
  if ((pars->obj[pars->obj_n[pars->ext]] = init_sphere()) == NULL)
    return (-1);
  return (0);
}

int		gt_cylindre(t_pars *pars)
{
  if (pars->end == 1)
    if (++pars->obj_n[pars->ext] > pars->nb_obj)
      return (my_puterror("Error. There is more object\
 in your file than you told us. Liar.\n"));
  if (pars->ext != OBJECT)
    return (my_puterror("Error, it's not an object' file !\n"));
  pars->type = CYLINDER;
  if ((pars->obj[pars->obj_n[pars->ext]] = init_cylinder()) == NULL)
    return (-1);
  return (0);
}

int		gt_cone(t_pars *pars)
{
  if (pars->end == 1)
    if (++pars->obj_n[pars->ext] > pars->nb_obj)
      return (my_puterror("Error. There is more object\
 in your file than you told us. Liar.\n"));
  if (pars->ext != OBJECT)
    return (my_puterror("Error, it's not an object' file !\n"));
  pars->type = CONE;
  if ((pars->obj[pars->obj_n[pars->ext]] = init_cone()) == NULL)
    return (-1);
  return (0);
}

int		gt_plan(t_pars *pars)
{
  if (++pars->obj_n[pars->ext] > pars->nb_obj)
    return (my_puterror("Error. There is more object\
 in your file than you told us. Liar.\n"));
  if (pars->ext != OBJECT)
    return (my_puterror("Error, it's not an object' file !\n"));
  pars->type = PLAN;
  if ((pars->obj[pars->obj_n[pars->ext]] = init_plan()) == NULL)
    return (-1);
  return (0);
}
