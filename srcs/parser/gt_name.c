/*
** gt_name.c for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs/parser
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Fri Jun  5 21:56:59 2015 Joffrey Mazier
** Last update Sun Jun  7 17:45:28 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include "define_file.h"
#include "rt1.h"

char		*my_str_dup(char *str)
{
  int		i;
  char		*tmp;

  i = 0;
  if ((tmp = malloc((sizeof(char)) * (my_strlen(str) + 2))) == NULL)
    return (NULL);
  while (str[i] != '\0')
    {
      tmp[i] = str[i];
      i++;
    }
  tmp[i] = '\0';
  return (tmp);
}

int		gt_name(t_pars *pars)
{
  char		*str;
  char		*name;

  if ((pars->ext == CAMERA || pars->ext == LUX) && pars->end == 1)
    if (++pars->obj_n[pars->ext] >= pars->nb_obj)
      return (my_puterror("Error. There is more object\
 in your file than you told us. Liar.\n"));
  if (pars->ext == OBJECT && pars->type == -1)
    return (my_puterror("No type for object !\n"));
  if ((str = get_next_line(pars->fd, 0)) == NULL)
    return (0);
  pars->end = 0;
  name = my_str_dup(str);
  free(str);
  if (pars->ext == OBJECT)
    pars->obj[pars->obj_n[pars->ext]]->name = name;
  else if (pars->ext == LUX)
    pars->spot[pars->obj_n[pars->ext]]->name = name;
  else if (pars->ext == CAMERA)
    pars->eye[pars->obj_n[pars->ext]]->name = name;
  pars->mask ^= O_NAME;
  return (0);
}
