/*
** display.c for rtV1 in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs/utils
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Mon Feb 23 22:04:39 2015 Gabriel CADET
** Last update Tue Mar 10 00:58:21 2015 Gabriel CADET
*/

#include <stdlib.h>
#include "rt1.h"
#include "xfiles.h"

void    aff_percent(int x)
{
  int   percent;

  percent = ((x + 1) * 100) / XWIN;
  my_putstrf("Please wait...\t", 1);
  my_putnbrf(percent, 1);
  my_putstrf(" %\r", 1);
}

int	my_strlen(char *str)
{
  int	i;

  if (!str)
    return (0);
  i = 0;
  while (str[i])
    ++i;
  return (i);
}

int	my_putcharf(char c, int fd)
{
  return (xwrite(fd, &c, fd));
}

int	my_putstrf(char *str, int fd)
{
  return (xwrite(fd, str, my_strlen(str)));
}

int	my_putnbrf(int nbr, int fd)
{
  int	ret;

  ret = 0;
  if (nbr < 0)
    {
      ret += my_putcharf('-', fd);
      if (nbr <= -10)
	ret += my_putnbrf(-(nbr / 10), fd);
      ret += my_putcharf(-(nbr % 10) + '0', fd);
    }
  else
    {
      if (nbr >= 10)
	ret += my_putnbrf(nbr / 10, fd);
      ret += my_putcharf(nbr % 10 + '0', fd);
    }
  return (ret);
}
