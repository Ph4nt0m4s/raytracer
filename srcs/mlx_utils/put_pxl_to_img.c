/*
** wall.c for Wolf3D in /home/cadet_g/tp/Graph/Wolf3D/srcs/V2
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Sat Dec 20 14:23:33 2014 Gabriel CADET
** Last update Tue Jul 05 13:42:57 2016 Gabriel CADET
*/

#include "mlx.h"
#include "rt1.h"

int	put_pxl_to_img(t_img *img, int x, int y, int color)
{
  int	colorval;
  char	*pixel;
  char	*rgb;
  int	i;

  i = -1;
  colorval = mlx_get_color_value(img->mlx, color);
  rgb = (char *) &colorval;
  pixel = img->addr + (x * img->bpp / 8) + (y * img->line);
  if (img->endian)
    while (((img->bpp / 8) - ++i) > 0)
      pixel[(img->bpp / 8) - i] = rgb[i];
  else
    while (((img->bpp / 8) - ++i) > 0)
      pixel[i] = rgb[i];
  return (0);
}
