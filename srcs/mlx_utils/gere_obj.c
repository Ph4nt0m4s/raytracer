/*
** gere_obj.c for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs/mlx_utils
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Fri Jun  5 21:11:43 2015 Joffrey Mazier
** Last update Sun Jun  7 13:45:12 2015 Joffrey Mazier
*/

#include <stdio.h>
#include <mlx.h>
#include "keymap.h"
#include "define_file.h"
#include "rt1.h"

void		gere_key_obj_translation(int key, t_pars *util)
{
  if (key == UP)
    util->obj[util->eve->select[0]]->z += 10;
  else if (key == DOWN)
    util->obj[util->eve->select[0]]->z -= 10;
  else if (key == LEFT)
    util->obj[util->eve->select[0]]->y += 10;
  else if (key == RIGHT)
    util->obj[util->eve->select[0]]->y -= 10;
  else if (key == P_UP)
    util->obj[util->eve->select[0]]->x += 10;
  else if (key == P_DOWN)
    util->obj[util->eve->select[0]]->x -= 10;
}

void		gere_key_obj_rotation(int key, t_pars *util)
{
  if (key == UP)
    rota_x_obj(util->obj[util->eve->select[0]], DEGTORAD(4));
  else if (key == DOWN)
    rota_x_obj(util->obj[util->eve->select[0]], DEGTORAD(-4));
  else if (key == LEFT)
    rota_y_obj(util->obj[util->eve->select[0]], DEGTORAD(4));
  else if (key == RIGHT)
    rota_y_obj(util->obj[util->eve->select[0]], DEGTORAD(-4));
  else if (key == P_UP)
    rota_z_obj(util->obj[util->eve->select[0]], DEGTORAD(4));
  else if (key == P_DOWN)
    rota_z_obj(util->obj[util->eve->select[0]], DEGTORAD(-4));
}

void		gere_limited(int key, t_pars *util)
{
  if (key == UP)
    util->obj[util->eve->select[0]]->limit[0] += 2;
  else if (key == DOWN)
    util->obj[util->eve->select[0]]->limit[0] -= 2;
}

void		gere_key_obj2(int key, t_pars *util)
{
  if (key == KEY_3)
    {
      if (util->obj[util->eve->select[0]]->limit[1] == 2)
        {
          printf("Limited mode disabled !\n");
          util->obj[util->eve->select[0]]->limit[1] = 0;
        }
      else
        {
          util->obj[util->eve->select[0]]->limit[1]++;
          printf("Limited mode enabled : mode %f!\n",
                 util->obj[util->eve->select[0]]->limit[1]);
          util->eve->mode = LIMITED;
        }
    }
  if (key == UP || key == DOWN || key == KEY_ESC ||
      key == LEFT || key == RIGHT || key == P_UP || key == P_DOWN
      || key == FASTER || key == NICER)
    {
      get_image(util->img, util->eye[util->img->cam], util->obj, util->spot);
      mlx_put_image_to_window(util->img->mlx,
                              util->img->win, util->img->img, 0, 0);
    }
}

void		gere_key_obj(int key, t_pars *util)
{
  if (util->eve->mode == ROTATION)
    gere_key_obj_rotation(key, util);
  else if (util->eve->mode == TRANSLATION)
    gere_key_obj_translation(key, util);
  else if (util->eve->mode == LIMITED)
    gere_limited(key, util);
  else if (util->eve->mode == RESIZE)
    gere_resize(key, util);
  if (key == TABUL)
    {
      if (util->eve->select[0] < util->nb_object - 1)
        util->eve->select[0]++;
      else
        util->eve->select[0] = 0;
      printf("Object n°%d\n", util->eve->select[0]);
      printf("Name : %s\n", util->obj[util->eve->select[0]]->name);
    }
  gere_key_obj2(key, util);
}
