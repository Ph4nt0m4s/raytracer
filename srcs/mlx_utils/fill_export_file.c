/*
** fill_export_file.c for raytracer
** in /home/mazier_j/MUL_2014_rtracer/srcs/mlx_utils
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Fri Jun  5 21:29:54 2015 Joffrey Mazier
** Last update Sat Jun  6 17:25:36 2015 Joffrey Mazier
*/

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "rt1.h"
#include "define_file.h"

void		write_in_file(t_pars *util, FILE *fd, int nb, int type)
{
  fprintf(fd, "## NAME\n");
  fprintf(fd, "%s\n", util->obj[nb]->name);
  fprintf(fd, "## POS\n");
  fprintf(fd, "%f\n%f\n%f\n",
          util->obj[nb]->x, util->obj[nb]->y, util->obj[nb]->z);
  fprintf(fd, "## VECTOR\n%f\n%f\n%f\n",
	  util->obj[nb]->vx, util->obj[nb]->vy, util->obj[nb]->vz);
  fprintf(fd, "## COLOR\n%x\n", util->obj[nb]->color);
  fprintf(fd, "## BRILLANCE\n");
  fprintf(fd, "%f\n", util->obj[nb]->brillance);
  if (type == CONE)
    fprintf(fd, "## EXTRA\n%f\n", (util->obj[nb]->extra * 180 / M_PI));
  else
    fprintf(fd, "## EXTRA\n%f\n", util->obj[nb]->extra);
  fprintf(fd, "## LIMIT\n%f\n", util->obj[nb]->limit[0]);
  fprintf(fd, "## LIMIT_MODE\n%f\n", util->obj[nb]->limit[1]);
  fprintf(fd, "## REFL\n%f\n", util->obj[nb]->refl);
  fprintf(fd, "## END\n\n");
}

int		find_out_type(t_pars *util, int nb, FILE *fd)
{
  int		type;

  type = 0;
  if (util->obj[nb]->inter == inter_sphere)
    {
      fprintf(fd, "## SPHERE\n");
      type = SPHERE;
    }
  else if (util->obj[nb]->inter == inter_plan)
    {
      fprintf(fd, "## PLAN\n");
      type = PLAN;
    }
  else if (util->obj[nb]->inter == inter_cone)
    {
      fprintf(fd, "## CONE\n");
      type = CONE;
    }
  else if (util->obj[nb]->inter == inter_cylinder)
    {
      fprintf(fd, "## CYLINDRE\n");
      type = CYLINDER;
    }
  return (type);
}

void		full_fill_obj(t_pars *util, FILE *fd)
{
  int		nb;
  int		type;

  nb = 0;
  fprintf(fd, "TYPE= OBJ\n");
  while (util->obj[nb] != NULL)
    nb++;
  fprintf(fd, "%s%d%s", "NB_OBJ= ", nb, "\n\n");
  nb = 0;
  while (util->obj[nb] != NULL)
    {
      type = find_out_type(util, nb, fd);
      write_in_file(util, fd, nb, type);
      nb++;
    }
}

void		full_fill_cam(t_pars *util, FILE *fd)
{
  int		nb;

  nb = 0;
  fprintf(fd, "TYPE= CAM\n");
  while (util->eye[nb] != NULL)
    nb++;
  fprintf(fd, "%s%d%s", "NB_OBJ= ", nb, "\n\n");
  nb = 0;
  while (util->eye[nb] != NULL)
    {
      fprintf(fd, "## NAME\n");
      fprintf(fd, "%s\n", util->eye[nb]->name);
      fprintf(fd, "## POS\n%f\n%f\n%f\n",
              util->eye[nb]->x, util->eye[nb]->y, util->eye[nb]->z);
      fprintf(fd, "## DIST\n%f\n", util->eye[nb]->dist);
      fprintf(fd, "## FOV\n%f\n", (util->eye[nb]->fov * 180 / M_PI));
      fprintf(fd, "## ANGLE\n%f\n%f\n%f\n",
              (util->eye[nb]->anglex * 180 / M_PI),
              (util->eye[nb]->angley * 180 / M_PI),
              (util->eye[nb]->anglez * 180 / M_PI));
      fprintf(fd, "## END\n\n");
      nb++;
    }
}

void		full_fill_lux(t_pars *util, FILE *fd)
{
  int		nb;

  nb = 0;
  fprintf(fd, "TYPE= LUX\n");
  while (util->spot[nb] != NULL)
    nb++;
  fprintf(fd, "%s%d%s", "NB_OBJ= ", nb, "\n\n");
  nb = 0;
  while (util->spot[nb] != NULL)
    {
      fprintf(fd, "## NAME\n");
      fprintf(fd, "%s\n", util->spot[nb]->name);
      fprintf(fd, "## POS\n%f\n%f\n%f\n",
              util->spot[nb]->x, util->spot[nb]->y, util->spot[nb]->z);
      fprintf(fd, "## COLOR\n%x\n", util->spot[nb]->color);
      fprintf(fd, "## END\n\n");
      nb++;
    }
}
