/*
** create_export_file.c for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs/mlx_utils
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Fri Jun  5 21:28:32 2015 Joffrey Mazier
** Last update Fri Jun  5 21:34:35 2015 Joffrey Mazier
*/

#include <stdio.h>
#include <stdlib.h>
#include "rt1.h"
#include "define_file.h"

int		create_obj(t_pars *util, char *dir_name)
{
  FILE		*fd;
  char		*file_name;

  file_name = "exp_obj.obj";
  if ((file_name = my_str_cat(dir_name, file_name)) == NULL)
    return (my_puterror("Error with my_str_cat() !\n"));
  if ((fd = fopen(file_name, "w")) == NULL)
    return (my_puterror("Error with open() !\n"));
  full_fill_obj(util, fd);
  fclose(fd);
  return (0);
}

int		create_cam(t_pars *util, char *dir_name)
{
  FILE		*fd;
  char		*file_name;

  file_name = "exp_cam.cam";
  if ((file_name = my_str_cat(dir_name, file_name)) == NULL)
    return (my_puterror("Error with my_str_cat() !\n"));
  if ((fd = fopen(file_name, "w")) == NULL)
    return (my_puterror("Error with open() !\n"));
  full_fill_cam(util, fd);
  fclose(fd);
  return (0);
}

int		create_lux(t_pars *util, char *dir_name)
{
  FILE		*fd;
  char		*file_name;

  file_name = "exp_lux.lux";
  if ((file_name = my_str_cat(dir_name, file_name)) == NULL)
    return (my_puterror("Error with my_str_cat() !\n"));
  if ((fd = fopen(file_name, "w")) == NULL)
    return (my_puterror("Error with open() !\n"));
  full_fill_lux(util, fd);
  fclose(fd);
  return (0);
}
