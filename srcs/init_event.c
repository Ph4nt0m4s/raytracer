/*
** init_event.c for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Fri Jun  5 22:42:45 2015 Joffrey Mazier
** Last update Sat Jun  6 21:55:30 2015 Joffrey Mazier
*/

#include "define_file.h"
#include "rt1.h"

void		init_events(t_pars *util)
{
  util->eve->select[0] = 0;
  util->eve->select[1] = MIN_MODE;
  util->eve->mode = TRANSLATION;
  util->img->faster = 1;
  util->img->cam = 0;
}
