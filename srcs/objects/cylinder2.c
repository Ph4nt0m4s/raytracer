/*
** cylinder2.c for raytracer in
**  /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs/objects
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Tue Feb 24 00:21:13 2015 Gabriel CADET
** Last update Fri Jun  5 22:53:22 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include "rt1.h"

int	cylinder_set_color(t_obj *cylinder, int color)
{
  cylinder->color = color;
  return (0);
}

int	cylinder_vector_x(t_obj *cylinder, double value)
{
  cylinder->vx = value;
  return (0);
}

int	cylinder_vector_y(t_obj *cylinder, double value)
{
  cylinder->vy = value;
  return (0);
}

int	cylinder_vector_z(t_obj *cylinder, double value)
{
  cylinder->vz = value;
  return (0);
}

int	cylinder_set_bril(t_obj *cylinder, double value)
{
  cylinder->brillance = value;
  return (0);
}
