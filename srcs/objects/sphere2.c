/*
** sphere2.c for raytracer in
** /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs/objects
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Tue Feb 24 00:21:13 2015 Gabriel CADET
** Last update Sat Jun  6 17:23:13 2015 Joffrey Mazier
*/

#include <stdio.h>
#include <stdlib.h>
#include "rt1.h"

int	sphere_set_color(t_obj *sphere, int color)
{
  sphere->color = color;
  return (0);
}

int	sphere_set_bril(t_obj *sphere, double value)
{
  sphere->brillance = value;
  return (0);
}
int	sphere_vector_x(t_obj *sphere, double value)
{
  sphere->vx = value;
  return (0);
}
int	sphere_vector_y(t_obj *sphere, double value)
{
  sphere->vy = value;
  return (0);
}

int	sphere_vector_z(t_obj *sphere, double value)
{
  sphere->vz = value;
  return (0);
}
