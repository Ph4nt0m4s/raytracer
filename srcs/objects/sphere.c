/*
** sphere.c for raytracer
** in /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs/objects
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Tue Feb 24 00:21:13 2015 Gabriel CADET
** Last update Sat Jun  6 14:26:10 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include "xfiles.h"
#include "rt1.h"

t_obj	*init_sphere()
{
  t_obj	*new_sphere;

  if (!(new_sphere = xmalloc(sizeof(t_obj))))
    return (NULL);
  new_sphere->set_x = sphere_set_x;
  new_sphere->set_y = sphere_set_y;
  new_sphere->set_z = sphere_set_z;
  new_sphere->vector_x = sphere_vector_x;
  new_sphere->vector_y = sphere_vector_y;
  new_sphere->vector_z = sphere_vector_z;
  new_sphere->set_color = sphere_set_color;
  new_sphere->set_bril = sphere_set_bril;
  new_sphere->set_extra = sphere_set_radius;
  new_sphere->inter = inter_sphere;
  new_sphere->get_normal = normal_sphere;
  new_sphere->destroy = destroy_objs;
  return (new_sphere);
}

int	sphere_set_x(t_obj *sphere, double value)
{
  sphere->x = value;
  return (0);
}

int	sphere_set_y(t_obj *sphere, double value)
{
  sphere->y = value;
  return (0);
}

int	sphere_set_z(t_obj *sphere, double value)
{
  sphere->z = value;
  return (0);
}

int	sphere_set_radius(t_obj *sphere, double value)
{
  sphere->extra = value;
  return (0);
}
