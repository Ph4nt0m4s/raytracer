/*
** plan2.c for raytracer in
** /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs/objects
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Tue Feb 24 00:21:13 2015 Gabriel CADET
** Last update Fri Jun  5 22:56:23 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include "rt1.h"

int	plan_vector_x(t_obj *plan, double value)
{
  plan->vx = value;
  return (0);
}

int	plan_vector_y(t_obj *plan, double value)
{
  plan->vy = value;
  return (0);
}

int	plan_vector_z(t_obj *plan, double value)
{
  plan->vz = value;
  return (0);
}

int	plan_set_bril(t_obj *plan, double value)
{
  plan->brillance = value;
  return (0);
}
