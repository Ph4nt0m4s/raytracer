/*
** inter_cylinder.c for Ray Tracer in
** /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/srcs/objects
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Sun Mar 22 19:13:57 2015 Gabriel CADET
** Last update Sun Jun  7 19:18:51 2015 Joffrey Mazier
*/

#include <math.h>
#include "rt1.h"

static double	get_a(t_eye *e, t_obj *cy, double cte)
{
  double	a;

  a = cte * (e->vx * e->vx + e->vy * e->vy + e->vz * e->vz) -
    (cy->vx * e->vx + cy->vy * e->vy + cy->vz * e->vz) *
    (cy->vx * e->vx + cy->vy * e->vy + cy->vz * e->vz);
  return (a);
}

static double	get_b(t_eye *e, t_obj *cy, double cte)
{
  double	b;

  b = 2.0 * (cte * ((e->x - cy->x) * e->vx + (e->y - cy->y) * e->vy
		    + (e->z - cy->z) * e->vz)
	     - (cy->vx * e->vx + cy->vy * e->vy + cy->vz * e->vz)
	     * (cy->vx * (e->x - cy->x) + cy->vy * (e->y - cy->y)
		+ cy->vz * (e->z - cy->z)));
  return (b);
}

static double	get_c(t_eye *e, t_obj *cy, double cte)
{
  double	c;

  c = cte * ((e->x - cy->x) * (e->x - cy->x)
	     + (e->y - cy->y) * (e->y - cy->y)
	     + (e->z - cy->z) * (e->z - cy->z)
	     - cy->extra * cy->extra)
    - (cy->vx * (e->x - cy->x) + cy->vy * (e->y - cy->y)
       + cy->vz * (e->z - cy->z)) * (cy->vx * (e->x - cy->x) + cy->vy
				     * (e->y - cy->y) + cy->vz
				     * (e->z - cy->z));
  return (c);
}

double		inter_cylinder(t_eye *e, t_obj *cy)
{
  double	a;
  double	b;
  double	c;
  double	cte;
  double	delta;
  double	arg[3];
  double	dist;

  cte = cy->vx * cy->vx + cy->vy * cy->vy + cy->vz * cy->vz;
  a = get_a(e, cy, cte);
  b = get_b(e, cy, cte);
  c = get_c(e, cy, cte);
  delta = b * b - 4.0 * a * c;
  dist = get_root(a, b, delta);
  arg[0] = a;
  arg[1] = b;
  arg[2] = delta;
  if (cy->limit[0] != 0 && cy->limit[1] != 0)
    return (inter_limited(e, cy, dist, arg));
  return (dist);
}
