/*
** make_objs.c for rtv1 in
** /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs/objects
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Sat Mar  7 09:32:15 2015 Gabriel CADET
** Last update Sun Jun  7 15:13:38 2015 Joffrey Mazier
*/

#include <stdlib.h>
#include "xfiles.h"
#include "rt1.h"

t_eye	*make_eye(double *coord, double dist, double fov, double *angle)
{
  t_eye	*eye;

  if (!(eye = init_eye()))
    return (NULL);
  eye->set_x(eye, coord[0]);
  eye->set_y(eye, coord[1]);
  eye->set_z(eye, coord[2]);
  eye->dist = dist;
  eye->fov = DEGTORAD(fov);
  eye->anglex = DEGTORAD(angle[0]);
  eye->angley = DEGTORAD(angle[1]);
  eye->anglez = DEGTORAD(angle[2]);
  return (eye);
}

t_obj	*make_sphere(double *coord, double *vector, int color, double *extra)
{
  t_obj	*sphere;

  (void) vector;
  if (!(sphere = init_sphere()))
    return (NULL);
  sphere->set_x(sphere, coord[0]);
  sphere->set_y(sphere, coord[1]);
  sphere->set_z(sphere, coord[2]);
  sphere->vector_x(sphere, vector[0]);
  sphere->vector_y(sphere, vector[1]);
  sphere->vector_z(sphere, vector[2]);
  sphere->set_bril(sphere, extra[0]);
  sphere->set_extra(sphere, extra[1]);
  sphere->set_color(sphere, color);
  return (sphere);
}

t_obj	*make_plan(double *coord, double *vector, int color, double *extra)
{
  t_obj	*plan;

  (void) extra;
  if (!(plan = init_plan()))
    return (NULL);
  plan->set_x(plan, coord[0]);
  plan->set_y(plan, coord[1]);
  plan->set_z(plan, coord[2]);
  plan->vector_x(plan, vector[0]);
  plan->vector_y(plan, vector[1]);
  plan->vector_z(plan, vector[2]);
  plan->set_color(plan, color);
  plan->set_bril(plan, extra[0]);
  return (plan);
}

t_obj	*make_cylinder(double *coord, double *vector,
		       int color, double *extra)
{
  t_obj	*cylinder;

  if (!(cylinder = init_cylinder()))
    return (NULL);
  cylinder->set_x(cylinder, coord[0]);
  cylinder->set_y(cylinder, coord[1]);
  cylinder->set_z(cylinder, coord[2]);
  cylinder->vector_x(cylinder, vector[0]);
  cylinder->vector_y(cylinder, vector[1]);
  cylinder->vector_z(cylinder, vector[2]);
  cylinder->set_bril(cylinder, extra[0]);
  cylinder->set_extra(cylinder, extra[1]);
  cylinder->set_color(cylinder, color);
  return (cylinder);
}

t_obj	*make_cone(double *coord, double *vector, int color, double *extra)
{
  t_obj	*cone;

  if (!(cone = init_cone()))
    return (NULL);
  cone->set_x(cone, coord[0]);
  cone->set_y(cone, coord[1]);
  cone->set_z(cone, coord[2]);
  cone->vector_x(cone, vector[0]);
  cone->vector_y(cone, vector[1]);
  cone->vector_z(cone, vector[2]);
  cone->set_bril(cone, extra[0]);
  cone->set_extra(cone, extra[1]);
  cone->set_color(cone, color);
  return (cone);
}
