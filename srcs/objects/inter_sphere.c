/*
** inter_sphere.c for Raytracer in
** /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Tue Feb 24 01:40:04 2015 Gabriel CADET
** Last update Sat Jun  6 15:05:50 2015 Joffrey Mazier
*/

#include <math.h>
#include "rt1.h"

double		get_root(double a, double b, double delta)
{
  double	k1;
  double	k2;

 if (delta < 0)
   return (-1);
  k1 = (-b - sqrt(delta)) / (2 * a);
  k2 = (-b + sqrt(delta)) / (2 * a);
  if (k1 < 0 || k2 < 0)
    return (MAX(k1, k2));
  else
    return (MIN(k1, k2));
}

double		get_root2(double a, double b, double delta)
{
  double	k1;
  double	k2;

 if (delta < 0)
   return (-1);
  k1 = (-b - sqrt(delta)) / (2 * a);
  k2 = (-b + sqrt(delta)) / (2 * a);
  if (k1 < 0 || k2 < 0)
    return (MIN(k1, k2));
  else
    return (MAX(k1, k2));
}

double		get_scal(t_eye *eye, double dist, t_obj *obj)
{
  double	vtmp[3];
  double	scal;
  double	norm;
  double	coord[3];

  coord[0] = eye->x + (eye->vx * dist);
  coord[1] = eye->y + (eye->vy * dist);
  coord[2] = eye->z + (eye->vz * dist);
  vtmp[0] = coord[0] - obj->x;
  vtmp[1] = coord[1] - obj->y;
  vtmp[2] = coord[2] - obj->z;
  norm = sqrt(obj->vx * obj->vx + obj->vy
	      * obj->vy + obj->vz *obj->vz);
  scal = (vtmp[0] * obj->vx + vtmp[1]
	  * obj->vy + vtmp[2] * obj->vz) / norm;
  return (scal);
}

double		inter_limited(t_eye *eye, t_obj *sphere, double dist,
				     double *arg)
{
  double	scal;

  scal = get_scal(eye, dist, sphere);
  if (sphere->limit[1] == 1)
    {
      if (scal < sphere->limit[0])
	{
	  dist = get_root2(arg[0], arg[1], arg[2]);
	  scal = get_scal(eye, dist, sphere);
	  if (scal < sphere->limit[0])
	    return (-1);
	}
    }
  else if (sphere->limit[1] == 2)
    {
      if (scal < 0 || scal > sphere->limit[0])
	{
	  dist = get_root2(arg[0], arg[1], arg[2]);
	  scal = get_scal(eye, dist, sphere);
	  if (scal < 0 || scal > sphere->limit[0])
	    return (-1);
	}
    }
  return (dist);
}

double		inter_sphere(t_eye *eye, t_obj *sphere)
{
  double	a;
  double	b;
  double	c;
  double	delta;
  double	dist;
  double	arg[3];

  a = eye->vx * eye->vx + eye->vy * eye->vy + eye->vz * eye->vz;
  b = (2 * ((eye->x - sphere->x) * eye->vx + (eye->y - sphere->y) * eye->vy
	    + (eye->z - sphere->z) * eye->vz));
  c = ((eye->x - sphere->x) * (eye->x - sphere->x)
       + (eye->y - sphere->y) * (eye->y - sphere->y)
       + (eye->z - sphere->z) * (eye->z - sphere->z)
       - sphere->extra * sphere->extra);
  delta = b * b - 4.0 * a * c;
  dist = get_root(a, b, delta);
  arg[0] = a;
  arg[1] = b;
  arg[2] = delta;
  if (sphere->limit[0] != 0 && sphere->limit[1] != 0)
    return (inter_limited(eye, sphere, dist, arg));
  return (dist);
}
