/*
** rotate_obj.c for raytracer in /home/mazier_j/MUL_2014_rtracer/srcs/objects
**
** Made by Joffrey Mazier
** Login   <mazier_j@epitech.net>
**
** Started on  Sat Jun  6 18:26:37 2015 Joffrey Mazier
** Last update Sat Jun  6 18:31:36 2015 Joffrey Mazier
*/

#include <math.h>
#include "rt1.h"

double		rota_x_obj(t_obj *obj, double angle)
{
  double	y_tmp;
  double	z_tmp;

  y_tmp = obj->vy;
  z_tmp = obj->vz;
  obj->vy = y_tmp * cos(angle) - z_tmp * sin(angle);
  obj->vz = y_tmp * sin(angle) + z_tmp * cos(angle);
  return (0);
}

double		rota_y_obj(t_obj *obj, double angle)
{
  double	x_tmp;
  double	z_tmp;

  x_tmp = obj->vx;
  z_tmp = obj->vz;
  obj->vz = z_tmp * cos(angle) - x_tmp * sin(angle);
  obj->vx = z_tmp * sin(angle) + x_tmp * cos(angle);
  return (0);
}

double		rota_z_obj(t_obj *obj, double angle)
{
  double	x_tmp;
  double	y_tmp;

  x_tmp = obj->vx;
  y_tmp = obj->vy;
  obj->vx = x_tmp * cos(angle) - y_tmp * sin(angle);
  obj->vy = x_tmp * sin(angle) + y_tmp * cos(angle);
  return (0);
}
