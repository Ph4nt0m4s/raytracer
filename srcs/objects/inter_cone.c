/*
** inter_cone.c for rtV1 in
** /home/cadet_g/rendu/Igraph/MUL_2014_rtv1/tp/srcs/objects
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Sat Mar  7 16:54:42 2015 Gabriel CADET
** Last update Sun Jun  7 19:16:55 2015 Joffrey Mazier
*/

#include <math.h>
#include "rt1.h"

static double	get_a(t_eye *e, t_obj *co, double cte)
{
  double	a;

  a = cte * (e->vx * e->vx
	     + e->vy * e->vy
	     + e->vz * e->vz - tan(co->extra)
	     * tan(co->extra) * (e->vx * e->vx
			       + e->vy * e->vy
			       + e->vz * e->vz))
    - (co->vx * e->vx + co->vy * e->vy + co->vz * e->vz)
    * (co->vx * e->vx + co->vy * e->vy + co->vz * e->vz);
  return (a);
}

static double	get_b(t_eye *e, t_obj *co, double cte)
{
  double	b;

  b = 2.0 * (cte * ((e->x - co->x) * e->vx
		    + (e->y - co->y) * e->vy
		    + (e->z - co->z) * e->vz - tan(co->extra)
		    * tan(co->extra) * ((e->x - co->x) * e->vx
					+ (e->y - co->y) * e->vy
					+ (e->z - co->z) * e->vz))
	     - (co->vx * e->vx + co->vy * e->vy + co->vz * e->vz )
	     * (co->vx * (e->x - co->x)
		+ co->vy * (e->y - co->y)
		+ co->vz * (e->z - co->z)));
  return (b);
}

static double	get_c(t_eye *e, t_obj *co, double cte)
{
  double	c;

  c = cte * ((e->x - co->x) * (e->x - co->x)
	     + (e->y - co->y) * (e->y - co->y)
	     + (e->z - co->z) * (e->z - co->z) - tan(co->extra)
	     * tan(co->extra) * ((e->x - co->x) * (e->x - co->x)
			       + (e->y - co->y) * (e->y - co->y)
			       + (e->z - co->z) * (e->z - co->z)))
    - (co->vx * (e->x - co->x) + co->vy * (e->y - co->y)
       + co->vz * (e->z - co->z)) * (co->vx * (e->x - co->x) + co->vy
				 * (e->y - co->y) + co->vz * (e->z - co->z));
  return (c);
}

double		inter_cone(t_eye *e, t_obj *co)
{
  double	a;
  double	b;
  double	c;
  double	cte;
  double	delta;
  double	dist;
  double	arg[3];

  cte = co->vx * co->vx + co->vy * co->vy + co->vz * co->vz;
  a = get_a(e, co, cte);
  b = get_b(e, co, cte);
  c = get_c(e, co, cte);
  delta = b * b - 4.0 * a * c;
  dist = get_root(a, b, delta);
  arg[0] = a;
  arg[1] = b;
  arg[2] = delta;
  if (co->limit[0] != 0 && co->limit[1] != 0)
    return (inter_limited(e, co, dist, arg));
  return (dist);
}
