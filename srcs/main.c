/*
** main.c for Wolf3D in /home/cadet_g/tp/Graph/Wolf3D/srcs
**
** Made by Gabriel CADET
** Login   <cadet_g@epitech.net>
**
** Started on  Tue Dec 16 17:48:56 2014 Gabriel CADET
** Last update Tue Jul 05 13:40:51 2016 Gabriel CADET
*/

#include <stdlib.h>
#include <X11/Xlib.h>
#include "mlx.h"
#include "xfiles.h"
#include "rt1.h"
#include "define_file.h"

t_img		*init_img()
{
  t_img		*img;

  if (!(img = xmalloc(sizeof(t_img))))
    return (NULL);
  if (!(img->mlx = mlx_init()))
    return (error_mlx());
  img->win = mlx_new_window(img->mlx, XWIN, YWIN, WIN_NAME);
  img->img = mlx_new_image(img->mlx, XWIN, YWIN);
  if (!img->win || !img->img)
    return (error_mlx());
  img->addr = mlx_get_data_addr(img->img, &img->bpp, &img->line, &img->endian);
  return (img);
}

t_eye		*create_eye(void)
{
  double	coord[3];
  double	angle[3];
  double	dist;
  double	fov;

  dist = 100;
  fov = 60;
  coord[0] = -300;
  coord[1] = 0;
  coord[2] = 0;
  angle[0] = 0;
  angle[1] = 0;
  angle[2] = 0;
  return (make_eye(coord, dist, fov, angle));
}

void		destroy_all(t_eye *eye, t_obj **obj, t_spot **spot)
{
  int		i;

  free(eye);
  i = -1;
  if (obj)
    {
      while (obj[++i])
	obj[i]->destroy(&obj[i]);
      free(obj);
      i = -1;
    }
  if (spot)
    {
      while (spot[++i])
	free(spot[i]);
      free(spot);
    }
}

int		raytracer(char *dir_path)
{
  t_img		*img;
  t_eye		**eye;
  t_obj		**objects;
  t_spot	**spots;
  t_pars	*util;

  objects = NULL;
  spots = NULL;
  eye = NULL;
  if (!(img = init_img()))
    return (-1);
  if ((util = init_parsing(dir_path, objects, spots, eye)) == NULL)
    return (my_puterror("Error while parsing your files. Fix your scene !\n"));
  if ((util->eve = malloc((sizeof(t_eve)) * 1)) == NULL)
    return (my_puterror("Error with malloc(), util->eve !\n"));
  eye = util->eye;
  objects = util->obj;
  spots = util->spot;
  util->img = img;
  init_events(util);
  get_image(img, eye[util->img->cam], objects, spots);
  mlx_key_hook(img->win, gere_key, util);
  mlx_expose_hook(img->win, gere_expose, util);
  mlx_loop(img->mlx);
  return (0);
}

int		main(int argc, char **argv)
{
  if (argc != 2)
    return (my_puterror("Error, invalid number of arguments. We only need\
your scene's directory's path ..\n"));
  if (raytracer(argv[1]))
    return (-1);
  return (0);
}
